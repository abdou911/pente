#pragma once
#include <boost/asio.hpp>
#include "../artificialInteligence/artificialInteligence.h"
#include "socketCom.h"

using boost::asio::ip::tcp;

/// AI that gets its information from a server
class ClientAI : public ArtificialIntelligence
{
public:
    std::string host;
    std::string port;
    boost::asio::io_context io_context;

    ClientAI(std::string portStr, std::string hostName = "localhost"): host(std::move(hostName)), port(std::move(portStr)), io_context()
    {};

    /// takes a grid, analyses it and returns a move
    Move getMove(const Board& board) override
    {
        try
        {
            // turns information about server into endpoints that can be contacted
            tcp::resolver resolver(io_context);
            tcp::resolver::results_type endpoints = resolver.resolve(host,port);

            // opens a connection to the server
            tcp::socket socket = connectServer(endpoints);

            // sends the board to the AI
            Com::toSocket(socket, board);

            // returns the move suggested by the AI
            return Com::fromSocket<Move>(socket);
        }
        catch (std::exception& e)
        {
            std::cerr << e.what() << std::endl;
            // returns an illegal move so that the error is seen as a loss and the program keeps going
            return Move(-1,-1);
        }
    };

private:
    /// connects to an endpoint, return a socket
    /// wait if the server has not been started yet
    tcp::socket connectServer(tcp::resolver::results_type& endpoints)
    {
        tcp::socket socket(io_context);

        // opens a connection to the server
        boost::system::error_code error;
        boost::asio::connect(socket, endpoints, error);

        if(error)
        {
            std::cout << "Waiting for AI connection on " << host << ':' << port << "..." << std::endl;

            // TODO add timeout ?
            while(error)
            {
                boost::asio::connect(socket, endpoints, error);
            }

            std::cout << "AI connected !" << std::endl;
        }

        return socket;
    }
};
